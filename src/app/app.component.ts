import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: Http, private httpClient: HttpClient) {
    // default options
    http
      .get('https://www.amazon.com/')
      .subscribe(noop, noop);

    // interceptor
    httpClient
      .get('https://ya.ru')
      .subscribe(noop, noop);
  }
}

function noop() {
  // nothing
}
