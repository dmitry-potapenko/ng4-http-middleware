import { Injectable } from '@angular/core';
import { BaseRequestOptions } from '@angular/http';

@Injectable()
export class RequestOptionsService extends BaseRequestOptions {
  constructor() {
    super();
    console.log('#RequestOptionsService was initialized');
    this.headers.set('Authorization', 'JWT code');
  }
}