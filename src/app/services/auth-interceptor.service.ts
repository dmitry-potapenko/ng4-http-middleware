import { Injectable } from '@angular/core';

import {
  HttpEvent, HttpInterceptor, HttpHandler,
  HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('@@@ Interceptor was called');
    // Clone the request to add the new header.
    const reqClone = req.clone({headers: req.headers.set('x-test-headers', 'asd32')});
    // Pass on the cloned request instead of the original request.
    // req.headers.set('X-TEST_HEADER', 'asd32');
    return next.handle(reqClone);
  }
}