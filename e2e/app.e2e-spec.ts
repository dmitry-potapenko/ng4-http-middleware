import { Ng4HttpHeadersPage } from './app.po';

describe('ng4-http-headers App', () => {
  let page: Ng4HttpHeadersPage;

  beforeEach(() => {
    page = new Ng4HttpHeadersPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
